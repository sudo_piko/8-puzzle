#!/usr/bin/env python
# 17.10.2020
# Ertel: Einführung in die KI
# S. 133, Aufgabe 6.6 a)

# Breiten- und Tiefensuche und Iterative Deepening programmieren.
# Bsp: 8-Puzzle.

# 8-Puzzle.

from __future__ import annotations
import random
import copy
from typing import Optional, List, Tuple

MAX_HEUR = 20


class Puzzle:
    def __init__(self):
        self.werte = [1, 2, 3, 4, 5, 6, 7, 8, 0]
        self.zuganzahl: int = 0
        self.richtungen = ["l", "r", "o", "u"]
        # self.versuchte_richtungen = []
        # self.bisherige_stellungen = bisherige_stellungen

    def ausgeben(self, leerzeile_danach=True, mit_zuganzahl=False, einezeile=False) -> None:
        if einezeile:
            print(self.werte)
            return
        if mit_zuganzahl:
            print("Anzahl der Züge:", self.zuganzahl)
        for wert in self.werte[:3]:
            print(wert, end="  ")
        print()
        for wert in self.werte[3:6]:
            print(wert, end="  ")
        print()
        for wert in self.werte[6:9]:
            print(wert, end="  ")
        print()

        if leerzeile_danach:
            print()

    def mischen(self) -> None:
        random.shuffle(self.werte)

    def setzen(self, neuer_zustand) -> None:
        self.werte = neuer_zustand

    @staticmethod
    def fehlermeldung() -> None:
        print("Der Spielfeldrand ist erreicht! Versuche eine andere Richtung.")

    def testen(self) -> bool:
        """Gibt True aus, wenn das Spiel gelöst wurde.
        """
        if self.werte == [1, 2, 3, 4, 5, 6, 7, 8, 0]:
            print("Gewonnen! Du hast {0} Züge gebraucht".format(self.zuganzahl))
            return True
        return False

    def tauschen(self, ort0: int, kommando: str) -> None:
        unerlaubtes = {"r": [2, 5, 8], "l": [0, 3, 6], "u": [6, 7, 8], "o": [0, 1, 2]}
        if ort0 in unerlaubtes[kommando]:
            Puzzle.fehlermeldung()
            return
        verschiebungswerte = {"r": 1, "l": -1, "o": -3, "u": 3}
        self.werte[ort0] = self.werte[ort0 + verschiebungswerte[kommando]]
        self.werte[ort0 + verschiebungswerte[kommando]] = 0

    def richtung_ist_moeglich(self, richtung: str) -> bool:
        """Überprüft, ob der Zug nach richtung über den Spielfeldrand hinaus gehen würde
        """
        unerlaubtes = {"r": [2, 5, 8], "l": [0, 3, 6], "u": [6, 7, 8], "o": [0, 1, 2]}
        if self.werte.index(0) in unerlaubtes[richtung]:
            return False
        return True

    def zug(self) -> Optional[bool]:
        ort: int = self.werte.index(0)
        richtung: str = input(
            "Zug {0}. In welche Richtung soll die 0 verschoben werden? u/o/r/l".format(self.zuganzahl + 1))
        if richtung not in self.richtungen:
            print("Keine Richtung angegeben. Mögliche Richtungen: u/o/r/l")
            return
        self.tauschen(ort, richtung)
        self.zuganzahl += 1
        self.ausgeben()
        return self.testen()

    def spiel(self) -> None:
        self.mischen()
        self.ausgeben()
        while True:
            status: bool = self.zug()
            if status:
                return


# AUTOMATISCHE SUCHE

class Autopuzzle(Puzzle):
    def __init__(self, werte=None, zuganzahl: int = 0, bisherige_stellungen=None):
        Puzzle.__init__(self)
        if werte is None:
            werte: List = [1, 2, 3, 4, 5, 6, 7, 8, 0]
        self.werte: List[int] = werte
        self.zuganzahl: int = zuganzahl
        if bisherige_stellungen is None:
            bisherige_stellungen = []
        self.bisherige_stellungen: List = bisherige_stellungen
        self.versuchte_richtungen: List[str] = []
        self.heuristischer_wert = 0

    def __str__(self) -> str:
        return str(self.werte)

    def groesser_als(self, other: Autopuzzle) -> bool:
        return self.heuristischer_wert > other.heuristischer_wert

    def erlaubte_richtung_finden(self) -> str:
        # gibt eine zufällige, aber erlaubte Richtung aus
        richtung: str = random.choice(self.richtungen)
        while not self.richtung_ist_moeglich(richtung):
            richtung = random.choice(self.richtungen)
        return richtung

    def autospiel_vorbereitung(self) -> None:
        self.mischen()
        print("Ausgangsbild: ")
        self.ausgeben()

# TIEFENSUCHE VERSION 1

    def autozug_tiefensuche1(self, richtung: str, einezeile: bool = False) -> Optional[Autopuzzle]:
        """Nimmt ein Puzzle und eine Richtung, führt einen zug durch und gibt ein neues Objekt aus."""
        # Kopien, damit das alte Objekt nicht verändert wird:
        neue_stellungen: List = copy.deepcopy(self.bisherige_stellungen)
        neue_werte = copy.deepcopy(self.werte)
        # ein neues Objekt
        neues_objekt = Autopuzzle(neue_werte, zuganzahl=self.zuganzahl, bisherige_stellungen=neue_stellungen)
        # Den aktuellen Zug der History hinzufügen
        neue_werte_dieses_zuges = copy.deepcopy(neues_objekt.werte)  # werte ist der derzeitige Zustand.
        neues_objekt.bisherige_stellungen.append((neue_werte_dieses_zuges, richtung))
        # in bisherige_stellungen wird die aktuelle Stellung und die angepeilte Richtung gespeichert.
        ort: int = neues_objekt.werte.index(0)
        # richtung = input("In welche Richtung soll die 0 verschoben werden? u/o/r/l")
        neues_objekt.tauschen(ort, richtung)
        neues_objekt.zuganzahl += 1
        # neues_objekt.ausgeben(einezeile=einezeile)
        if neues_objekt.testen():
            print("Lösung erreicht! {0} Züge nötig.".format(neues_objekt.zuganzahl))
            return
        # print("autozug_tiefensuche1 generiert:", neues_objekt)
        return neues_objekt

    def autoloesen_tiefensuche1(self, zuganzahltiefe: int, maxtiefe: int = 19) -> None:
        """Testet die Zuganzahl, sucht erlaubte Richtungen und gibt für jede einen Zug in Auftrag"""
        if zuganzahltiefe > maxtiefe:
            print("%%%%%%%%%%%%%%%%%%%%%%%% Keine Lösung nach {0} Zügen.".format(maxtiefe + 1))
            return
        print("Derzeitige Zuganzahl {0}".format(zuganzahltiefe))

        for richtung in self.richtungen:  # alle Richtungen durchgehen
            if self.richtung_ist_moeglich(richtung) and \
                    (self.werte, richtung) not in self.bisherige_stellungen:
                # Wenn die Richtung im Spielfeld bleibt und der Zug noch nicht vorgekommen ist:
                print("   Zug {0}, Richtung {1}".format(self.zuganzahl + 1, richtung))
                neu: Autopuzzle = self.autozug_tiefensuche1(richtung)  # , einezeile=True)
                neu.autoloesen_tiefensuche1(zuganzahltiefe=zuganzahltiefe + 1, maxtiefe=maxtiefe)
                self.versuchte_richtungen.append(richtung)
            else:  # Wenn er nicht möglich ist oder schon vorgekommen ist:
                self.versuchte_richtungen.append(richtung)
            if len(self.versuchte_richtungen) > 3:
                # Wenn alle Richtungen schon durchgetestet wurden und nichts klappt:
                print("Alle Richtungen ungültig oder redundant. Wähle zufälligen Zug.")
                richtung: str = self.erlaubte_richtung_finden()  # Eine zufällige, aber mögliche Richtung.
                print("     Zug {0}, Richtung {1}".format(self.zuganzahl + 1, richtung))
                neu: Autopuzzle = self.autozug_tiefensuche1(richtung)  # , einezeile=True)
                neu.autoloesen_tiefensuche1(zuganzahltiefe + 1, maxtiefe=maxtiefe)

    def iterative_deepening1(self, obere_grenze):
        """Führt autoloesen_tiefensuche1 mit Iterative Deepening durch. Von zwei Zügen bis obere_grenze."""
        for grenze in range(1, obere_grenze):
            # Großer Block, um im Output die Erhöhung von grenze wiederzufinden.
            print("""***************************************************************************************
            
            NEUE GRENZE: {0} — Abbruch nach {1} Zügen.
            
            """.format(grenze + 1, grenze + 1))
            # autoloesen. Zuganzahltiefe = 0 bedeutet, dass er von vorn anfängt.
            # maxtiefe ist, wie lange gesucht wird.
            self.autoloesen_tiefensuche1(zuganzahltiefe=0, maxtiefe=grenze)

    def autospiel_tiefensuche1(self, iterative_deepening: bool = False) -> None:
        self.autospiel_vorbereitung()
        if iterative_deepening:
            self.iterative_deepening1(20)
        else:
            self.autoloesen_tiefensuche1(0)

    def nachfolgerliste(self, debug=False) -> List:
        """Gibt eine Liste aller möglichen folgenden Zustände aus.
        """
        alle_moeglichen_nachfolger: List = []
        for richtung in self.richtungen:
            if self.richtung_ist_moeglich(richtung):  # nur Spielfeldrand
                neues_objekt: Autopuzzle = copy.deepcopy(self.autozug_tiefensuche1(richtung))
                # vermutlich ist das Problem autozug_tiefensuche1
                # print("neues objekt in nachfolgerliste()")
                # neues_objekt.ausgeben()
                alle_moeglichen_nachfolger.append(neues_objekt)  # hund begraben?
        # bisher generierte mögliche Nachfolger ausgeben:
        if debug:
            print("moegliche nachfolger: ", end="")
            for objekt in alle_moeglichen_nachfolger:
                print(objekt, end=" ")
            print()
        return alle_moeglichen_nachfolger

    def tiefensuche_rtl(self) -> str:  # wie bei Ertel, S. 108
        """Führt eine Tiefensuche durch.
        """
        if self.testen():
            print("Lösung gefunden.")
            return "Gefunden!"
        neue_objekte: List[Autopuzzle] = self.nachfolgerliste()
        while len(neue_objekte) > 0:
            ergebnis: str = neue_objekte.pop(0).tiefensuche_rtl()
            if ergebnis == "Gefunden!":
                return ergebnis
        return "Keine Lösung."

    def autospiel_tiefensuche_rtl(self) -> None:
        """Mischt das Spielfeld und führt eine Tiefensuche durch.
        """
        self.autospiel_vorbereitung()
        self.tiefensuche_rtl()

# BREITENSUCHE VERSION 1

    def eigene_breitensuche(self, knotenliste=None, debug: bool = True) -> Optional[Autopuzzle]:
        if knotenliste is None:
            knotenliste = []
        if debug:
            print("Knotenliste: ", end="")
            for knoten in knotenliste:
                print(knoten, end=", ")
            print()
        listenmaximun = 10
        if self.testen():
            print("Lösung gefunden.")
            return
        knotenliste += self.nachfolgerliste()
        if len(knotenliste) > listenmaximun:
            print("Zu viele Knoten!")
            return
        while len(knotenliste) > 0:
            erster_knoten = knotenliste.pop(0)
            ergebnis = erster_knoten.eigene_breitensuche(knotenliste)
            return ergebnis
        print("Keine Lösung.")
        return

    def autospiel_breitensuche(self) -> None:
        """Mischt das Spielfeld und führt eine Breitensuche durch.
        """
        self.autospiel_vorbereitung()
        self.eigene_breitensuche()

# HEURISTISCHE SUCHE

    def heuristik1(self) -> int:
        """Gibt die Anzahl der Plättchen, die nicht am richtigen Ort sind, zurück
        """
        ausgabe: int = 0
        for n in range(1, 9):  # zahlen von 1 bis 9
            if self.werte[n-1] != n:  # 0 wird nicht überprüft, weil die automatisch
                # am richtigen Ort ist, wenn alle anderen richtig sind.
                ausgabe += 1
        return ausgabe

    def heuristik2(self) -> int:
        """Gibt die Summe der Manhatten-Abstände aller Plättchen zum jeweils richtigen Ort zurück
        """
        ausgabe: int = 0
        for n in range(1, 9):  # zahlen von 1 bis 8
            if self.werte[n-1] != n:  # wenn das n-te Plättchen nicht am richtigen Platz ist,
                ausgabe += self.manhattan(n)  # soll der Manhattan abstand zurückgegeben werden.
        return ausgabe

    def koordinaten(self, zahl) -> Tuple:
        """Gibt die Koordinaten der Zahl als Tupel zurück, mittig oben ist (1,0)
        """
        ort: int = self.werte.index(zahl)
        x: int = ort % 3
        y: int = ort // 3
        return x, y

    def manhattan(self, zahl) -> int:
        """Gibt den Manhattan-Abstand eines Plättchen vom richtigen Ort zurück
        """
        koor = self.koordinaten(zahl)  # die jetzigen Koordinaten
        dx = abs(koor[0] - (zahl - 1) % 3)  # die Entfernung zum richtigen Ort auf der x-Achse
        dy = abs(koor[1] - (zahl - 1) // 3)  # und auf der y-Achse
        abstand = dx + dy
        return abstand

    def heuristisch_einsortieren(self, nachfolgerliste: List, debug: bool = False) -> List:
        """Gibt eine neue knotenliste zurück, in die die Nachfolgerknoten von self entsprechend
         der Heuristik-Funktion heuristik eingeordnet sind
        """
        # Ist es klüger, das über ein Attribut von Autopuzzle zu machen, oder über ein Dictionary?
        neue_nachfolger = self.nachfolgerliste()  # neue Knoten generieren
        if False:  # einmal alle Nachfolger zeigen
            for nachfolger in neue_nachfolger:
                print("neue Nachfolger:", nachfolger)
        for nachfolger in neue_nachfolger:  # für alle Nachfolgerknoten:
            nachfolger.heuristischer_wert = nachfolger.heuristik1()  # den heuristischen Wert berechnen
            print("neue Nachfolger:", nachfolger, nachfolger.heuristischer_wert)
            if nachfolgerliste == []:
                nachfolgerliste.append(nachfolger)
                print("Nachfolgerliste war leer, befülle mit einem Objekt.")
            elif nachfolger in self.versuchte_richtungen or nachfolger in nachfolgerliste:
                pass
            else:
                for listenelement in nachfolgerliste:  # die Queue durchgehen, bis:
                    if not nachfolger.groesser_als(listenelement):  # ein Listenelement größer als nachfolger ist. ->DAVOR!
                        stelle = nachfolgerliste.index(listenelement)  # die Stelle des Listenelements finden
                        nachfolgerliste[stelle:stelle] = [nachfolger]  # DAVOR einordnen
                        break
        return nachfolgerliste

    def heuristische_suche(self, knotenliste=None, debug: bool = True, max_heur=MAX_HEUR) -> Optional[Autopuzzle]:
        """Heuristische Suche über ein Dictionary
        """
        if knotenliste is None:
            knotenliste: List = []
        if debug:
            print("Zustand:", self.ausgeben())
            print("Knotenliste: \n  ", len(knotenliste), end="")
            for knoten in knotenliste:
                print(knoten, end=", ")
            print()
        listenmaximun = 30
        if self.testen():   # brauche ich das überhaupt? autozug_tiefensuche1 testet schon
            print("Lösung gefunden.")
            return
        # knotendic += self.nachfolgerliste()
        knotenliste: List = self.heuristisch_einsortieren(knotenliste, debug=debug)
        print("Länge der Knotenliste:", len(knotenliste))
        for i in knotenliste:
            print(i, i.heuristischer_wert)
        if len(knotenliste) > listenmaximun:
            print("Zu viele Knoten!")
            knotenliste = knotenliste[:20]
            # return
        while len(knotenliste) > 0:
            erster_knoten: Autopuzzle = knotenliste.pop(0)
            print("Heuristischer Wert:", erster_knoten.heuristischer_wert)
            ergebnis = erster_knoten.heuristische_suche(knotenliste, debug=debug, max_heur=max_heur)
            return ergebnis
        print("Keine Lösung.")
        return


if __name__ == "__main__":
    einpuzzle = Autopuzzle()
    einpuzzle.autospiel_vorbereitung()
    einpuzzle.heuristische_suche(debug=True)
    # einpuzzle.spiel()

# Derzeitiges Problem:
# Pendelt.
# Ich habe keine Schokolade.
