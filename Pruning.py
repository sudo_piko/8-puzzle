# 12.11.2020
# Alpha-Beta-Pruning, wie am Ende von Kapitel 6 im Ertel
# Die Alpha-Beta-Optimierung funktioniert problemlos, aber dieses Pruning verstehe ich nicht.
# Dafür müsste ich S. 126 verstehen.

import random
from typing import List


def zufallsliste(laenge) -> List[int]:
    liste: List = []
    for i in range(2**laenge):
        liste.append(random.randint(-100, 100))
    return liste


def maximaliste(eingabeliste) -> List[int]:
    ausgabeliste: List = []
    for i in range(0, len(eingabeliste), 2):
        ausgabeliste.append(max(eingabeliste[i], eingabeliste[i+1]))
    return ausgabeliste


def minimaliste(eingabeliste) -> List[int]:
    ausgabeliste: List = []
    for i in range(0, len(eingabeliste), 2):
        ausgabeliste.append(min(eingabeliste[i], eingabeliste[i+1]))
    return ausgabeliste


def minmax(eingabeliste, max_anfangen: bool = True, indent=0) -> int:
    schoene_listenausgabe(eingabeliste, indent, max_anfangen)
    if len(eingabeliste) > 1:
        if max_anfangen:
            eingabeliste = maximaliste(eingabeliste)
        else:
            eingabeliste = minimaliste(eingabeliste)
        max_anfangen = not max_anfangen
        indent += 1
        eingabeliste = minmax(eingabeliste, max_anfangen, indent)
    return eingabeliste


def schoene_listenausgabe(liste, indent, ist_max) -> None:
    if indent == 0:
        runde = "   "
    elif ist_max: runde = "min"
    else: runde = "max"
    print(runde, end=" : ")
    i = 0
    if indent > 0:
        print("  " * indent**2, end="")
    for zahl in liste:
        print("  "*indent +str(zahl).ljust(4+indent*4), end=" ")
        i += 1
        if i % 2 == 0:
            print("| ", end="")
    print()
    print()


minmax(zufallsliste(4))

